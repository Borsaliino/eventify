#this is a place where you should define your tables.
#add it to the TOP, right AFTER the connection.cursor() call.

#TABLES (add your sqlite table to this list)
#-users
#-friends
#-events
#-photos
#-photo_comments
#-sessions

import sqlite3, os

#delete any broken database
os.remove('sitedatabase.db')

print "Rebuilding database from scratch...Please Wait."

#create a new one
connection = sqlite3.Connection('sitedatabase.db')

cursor = connection.cursor()


#add users table
cursor.execute("""
CREATE TABLE users
(
userid INTEGER PRIMARY KEY AUTOINCREMENT,
username text UNIQUE,
email text UNIQUE,
firstname text,
lastname text,
password text
);""")

#add friends table
cursor.execute("""
CREATE TABLE friends
(
userid INTEGER,
friendid INTEGER,
PRIMARY KEY (userid,friendid),
FOREIGN KEY (userid) REFERENCES users (userid),
FOREIGN KEY (friendid) REFERENCES users (userid)
);""")


#add events table
cursor.execute("""
CREATE TABLE events
(
eventid INTEGER PRIMARY KEY AUTOINCREMENT,
eventname text,
description text,
date text,
venue text,
hostid INTEGER,
FOREIGN KEY (hostid) REFERENCES users (userid)
);""")

#add eventtags table
cursor.execute("""
CREATE TABLE eventtags
(
eventtagid INTEGER PRIMARY KEY AUTOINCREMENT,
eventid INTEGER,
tag TEXT,
FOREIGN KEY (eventid) REFERENCES events (eventid)
);""")

#add attendees table
cursor.execute("""
CREATE TABLE attendees
(
userid INTEGER,
eventid INTEGER,
PRIMARY KEY (userid, eventid),
FOREIGN KEY (userid) REFERENCES users (userid),
FOREIGN KEY (eventid) REFERENCES event (eventid)
);""")


#add photos table
cursor.execute("""
CREATE TABLE photos
(
photoid INTEGER PRIMARY KEY,
upvote int,
filepath text,
description text,
userid int,
eventid int,
FOREIGN KEY (userid) REFERENCES users (userid)
);""")

#add phototags table
cursor.execute("""
CREATE TABLE phototags
(
phototagid INTEGER PRIMARY KEY AUTOINCREMENT,
photoid INTEGER,
tag TEXT,
FOREIGN KEY (photoid) REFERENCES photos (photoid)
);""")

#add photo_comments table
cursor.execute("""
CREATE TABLE photo_comments
(
photocommentid INTEGER PRIMARY KEY,
data text,
userid int,
photoid int,
FOREIGN KEY (userid) REFERENCES users (userid),
FOREIGN KEY (photoid) REFERENCES photos (photoid)
);""")
# add sessions table
cursor.execute("""
CREATE TABLE sessions
(
sessionid INTEGER PRIMARY KEY,
uid INTEGER
);""")

#########################################################################
#this is some sample code to create a table
#cursor.execute('CREATE TABLE (Inserttablenamehere) (insertschemahere)')
