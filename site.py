from tornado import Server
from profile import profile_pages
from photos import photohandler
from event import event_pages
from user import user_pages
from sessions import sessions
from photos import photoAPI
import TemplateAPI
import database_engine, TemplateAPI
from profile import profile_pages
import search


database_engine.Init_Database()

server = Server()

def index(response):
    try:
        page = TemplateAPI.render('index.tem', response, {'getRandPhoto': photoAPI.Photo_GetRandom})
        response.write(page)
    except Exception, e:
        print e
        response.redirect("/internal_error")

def temp_styles(r):
	r.write(TemplateAPI.render('styletests.tem', r, {}))
server.register('/styletest', temp_styles)

def ServerError(response):
	response.write(TemplateAPI.render('error.html', response, {'er' : response.get_field('error')}))
    
def sitemap(response):
    response.write(TemplateAPI.render("sitemap.html", response, {}))
    

server.register("/", index)
server.register("/internal_error", ServerError)
server.register("/sitemap", sitemap)
photohandler.Init(server)


user_pages.page_init(server)
event_pages.page_init(server)
profile_pages.init_pages(server)
search.init_pages(server)

def search(response):
        response.write(TemplateAPI.render("search.html", response, {}))
    
server.register("/search",search)

def error404(response):
    response.write(TemplateAPI.render('error.html', response, {'er' : "404 Error!"}))
    

#photo URL handlers


server.register("/.*", error404)
server.run()
