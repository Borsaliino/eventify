import sys
sys.path.append("../")

import usertools
import TemplateAPI
import re
from sessions import sessions

def login(response):
    """serves the basic login page"""
    if sessions.get_uid(response) is not None:
        response.redirect("/")
        return
    response.write(TemplateAPI.render('login.html', response, {}))

def is_email(s):
    if re.match(r'^[0-9a-zA-Z_.]+@[0-9a-zA-Z_]+\.[0-9a-zA-Z_.]+[0-9a-zA-Z_]$', s):
        return True
    else:
        return False

def login_redirect(response):
    """checks login details and redirects"""
    user = response.get_field("user")
    password = response.get_field("password")
    print "%r"%password, type(password)
    user_dict = usertools.get_user(user)
    if user_dict and usertools.login(user_dict.username, password):
        sessions.login(response, user_dict.userid)
        response.redirect("/")
    else:
        response.redirect("/login_failed")

def login_failed(response):
    response.write(TemplateAPI.render('login_failed.html', response, {}))
        
def registration(response):
    """serves the basic registration page"""
    response.write(TemplateAPI.render('registration.html', response, {"defaults" : {}}))

def registration_action(response):
    """stores the registration info"""
    username = response.get_field("user")
    firstname = response.get_field("firstname")
    lastname = response.get_field("lastname")
    email = response.get_field("email")
    if email is None:
        email = ""
    password = response.get_field("password")
    password2 = response.get_field("password2")
    default = {"user":username, "firstname":firstname, "lastname":lastname, "email":email }
    for i in default:
        default[i] = default[i] if default[i] else ""
    if not is_email(email):
        default["error"] = "Email address is not valid"
    elif password != password2:
        default["error"] = "Passwords do not match"
    elif password == "" or password is None:
        default["error"] = "Password is not valid"
    elif usertools.get_user_from_email(email):
        default["error"] = "Email already in use"
    elif not firstname:
        default["error"] = "You must enter a first name"
    elif not lastname:
        default["error"] = "You must enter a last name"
    elif not username:
        default["error"] = "You must enter a username"
    else:
        user = usertools.create_user(username, firstname, lastname, email, password)
        if not user:
            default["error"] = "Username already in use"
        else:
            sessions.login(response, user.userid)
            response.redirect("/sregistration")
    if "error" in default:
        #response.redirect("/registration")
        response.write(TemplateAPI.render('registration.html', response, {"defaults":default}))

def success_registration(response):
    response.write(TemplateAPI.render('success_registration.html', response, {}))
    
def logout(response):
    uid = sessions.get_uid(response)
    user = usertools.get_user(uid)
    sessions.logout(response)
    response.redirect("/")

def logout_test(response):
    uid = sessions.get_uid(response)
    user = usertools.get_user(uid)
    response.write(TemplateAPI.render("logout.html", response, {"user": user}))

    
       

    
def page_init(server):
    server.register("/login",login)
    server.register("/registration",registration)
    server.register("/registration_action",registration_action)
    server.register("/login_redirect",login_redirect)
    server.register("/logout",logout)
    server.register("/login_failed",login_failed)
    server.register("/sregistration", success_registration)
