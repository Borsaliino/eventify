import sys
sys.path.append("../")

from search import searchHTMLrenderer

def init_pages(server):
    server.register("/search_page", searchHTMLrenderer.render_search)
    server.register("/search", searchHTMLrenderer.render_search_test)
    pass
