#This is the modulous to make python look one folder above the current folder
import sys
from random import randint
sys.path.append("../")

import database_engine

#ALL INTERNAL FUNCTIONS HAVE A PREFIX OF '_photo_'

def _photo_ListPhotosOfEvent( eventid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT photoid FROM photos WHERE eventid=?;""", (int(eventid),))
    out = []
    for x in curs.fetchall():
        out.append(x[0])
    return out



def _photo_GetImageURL(photoid):
    #query database for the file path here.
    return "/static/photodata/" + str(photoid) + ".jpg"

def _photo_CommitPicture(photoid, data):
    filehnd = open("static/photodata/" + str(photoid) + ".jpg", "wb")
    filehnd.write(data)
    filehnd.close()

def _photo_GetAll():
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT photoid FROM photos""")
    out = []
    for x in curs.fetchall():
        out.append(x[0])
    return out

def Photo_GetRandom(dummyvar=""):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""select count(*) from photos;""")
    try:
        return str(_photo_GetImageURL(randint(1,curs.fetchone()[0])))
    except:
        return "/static/photodata/1.jpg"

    
def _photo_CreatePhotoEntry(userid, eventid, description, filename):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""INSERT INTO photos(upvote, filepath, description, userid, eventid) values (0, ?, ?, ?, ?)""", (filename, description, userid, eventid))
    conn.commit()
    return curs.lastrowid


def GetUserID( photoid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT userid FROM photos WHERE photoid=?;", (int(photoid),))
    try:
        return curs.fetchone()[0]
    except:
        return -1

def GetEventID( photoid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT eventid FROM photos WHERE photoid=?;", (int(photoid),))
    try:
        return curs.fetchone()[0]
    except:
        return -1

def GetPhotosCreatedByUser( userid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT photoid FROM photos WHERE userid=?;", (int(userid),))
    out = []
    for x in curs.fetchall():
        out.append(x[0])
    return out

def GetusernameFromUID( userid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT username FROM users WHERE userid=?;", (int(userid),))
    try:
        return curs.fetchone()[0]
    except:
        return "[User Deleted]"

def GetEventnameFromID( eventid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT eventname FROM events WHERE eventid=?;", (int(eventid),))
    try:
        return curs.fetchone()[0]
    except:
        return "[Event Deleted]"

def GetDescription( photoid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT description FROM photos WHERE photoid=?;", (int(photoid),))
    try:
        return curs.fetchone()[0]
    except:
        return ""

def ListPhotosOfEvent( eventid):
    return _photo_ListPhotosOfEvent( eventid)

def GetPicIDFromUserID( userid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT photoid FROM photos WHERE userid=?;", (int(userid),))
    try:
        return curs.fetchone()[0]
    except:
        return -1


def GetDP( userid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("SELECT photoid FROM photos WHERE userid=? AND eventid=NULL;", (int(userid),))
    try:
        return curs.fetchone()[0]
    except:
        return -1

#COMMENTING SYSTEM

def _photo_GetPicComments(photoid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT * FROM photo_comments WHERE photoid=? order by photocommentid""", (int(photoid),))
    out = []
    for x in curs.fetchall():
        out.append((x[1],GetusernameFromUID(x[2])))
    return out

def Photo_WriteComment(photoid, message, userid):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""INSERT INTO photo_comments(data, userid, photoid) values (?, ?, ?)""", (message, int(userid), photoid))
    conn.commit()
    return curs.lastrowid
