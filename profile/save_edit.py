import sys
sys.path.append("../")

from user import usertools
import TemplateAPI
from sessions import sessions
import database_engine

def _save_edit(response):
        FN = response.get_field("firstname")
        
        LN = response.get_field("lastname")
        EM = response.get_field("email")
        uid = sessions.get_uid(response)

        if FN == None or LN == None or EM == None:
                response.redirect('/editprofile')

        else:
                conn = database_engine.Get_DatabaseConnection()
                curs = conn.cursor()
                curs.execute('''UPDATE users SET firstname=?, lastname=?, email=? WHERE userid=?''', (FN, LN, EM, uid))
                conn.commit()
                response.redirect('/profile')
