import sys
sys.path.append('../')
from profile import profile_page, editprofile_page, profile_redirect_page, save_edit

def init_pages(server):
    server.register("/profile", profile_redirect_page.redirect)
    server.register("/profile/([0-9]+)", profile_page.profile)
    server.register("/editprofile", editprofile_page.editprofile)
    server.register("/saveprofile", save_edit._save_edit)
    server.register("/add_friend", profile_page._profile_make_friends)
