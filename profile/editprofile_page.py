#This is the modulous to make python look one folder above the current folder
import sys
sys.path.append("../")

from user import usertools
import TemplateAPI
from sessions import sessions



def editprofile_form(response):
        uid = sessions.get_uid(response)
        if uid is not None:
                user = usertools.get_user(uid)
                response.write(TemplateAPI.render("editProfile.html", response, {"user" : user}))
        else:
              response.redirect('/')  

#-----------------------------------------
#Called when the page is loaded
def editprofile(response):
        user = sessions.get_uid(response)
        if response.get_field("Realname"):
                name = response.get_field("Realname")
                _save_edit(response, user)

        elif response.get_field("Pass"):
                passwd = response.get_field("Pass")
                _save_edit(response, user)

        elif response.get_field("UserName"):
                uName = response.get_field("UserName")
                _save_edit(response, user)
        else:
                editprofile_form(response)
        
